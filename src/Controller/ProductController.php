<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{

    #[Route('/products', name: 'app_products', methods: ['GET'])]
    public function index(ProductRepository $productRepo): Response
    {
        $data = $productRepo->findAll();

        return $this->json([
            "data"=>$data
        ]);
    }

    #[Route('/products/search', name: 'app_products_search', methods: ['GET'])]
    public function search(ProductRepository $productRepo, Request $request): Response
    {
        $keyword = $request->get('q');
        $data = $productRepo->findByKeyword($keyword);
        
        return $this->json([
            "data"=>$data
        ]);
    }
}
