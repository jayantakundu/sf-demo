FROM php:8.2-fpm

# copying the source directory and install the dependencies with composer
RUN mkdir -p /var/www/html

COPY . /var/www/html
WORKDIR /var/www/html

RUN apt-get update && apt-get install -y --no-install-recommends \
        locales apt-utils git zip unzip wget curl bash

RUN pecl install apcu && docker-php-ext-enable apcu && docker-php-ext-enable opcache

RUN docker-php-ext-install mysqli && \
    docker-php-ext-install pdo_mysql && \
    pecl install redis && \
    docker-php-ext-enable redis

# INSTALL COMPOSER
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN curl -sS https://get.symfony.com/cli/installer | bash && mv /root/.symfony5/bin/symfony /usr/local/bin/symfony

# run composer install to install the dependencies
RUN composer install \
 --optimize-autoloader \
 --no-interaction

# Expose the port
EXPOSE 8000

# Lets start app
CMD ["symfony", "server:start"]