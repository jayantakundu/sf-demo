<?php

namespace Guidepoint\Bundle\ComplianceBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;
use Guidepoint\Bundle\ComplianceBundle\Entity\DoNotContact;

class DncManager
{
    protected $entityManager;
    protected $dncRepository;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
        $this->dncRepository = $this->entityManager->getRepository(DoNotContact::class);
    }

    public function getDncCompanyData(Request $request) {
        
        $result = $this->dncRepository->findBy(
            array(),
            array('id' => 'DESC'),
            10,
            0
        );
        
        return $result;
    }

}