<?php

namespace Guidepoint\Bundle\ComplianceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="DoNotContact")
 */
class DoNotContact
{
    /**
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *  @ORM\Column(name="CompanyName", type="string", length=255, nullable=false)
     */
    protected $companyName;

    /**
     *  @ORM\Column(name="CompanyNameAlternateList", type="string", length=5000, nullable=true)
     */
    protected $companyNameAlternateList;

    /**
     *  @ORM\Column(name="CreationDateTime", type="datetime", nullable=false)
     */
    protected $creationDateTime;

    /**
     *  @ORM\Column(name="EnteredByPersonID", type="integer", nullable=true)
     */
    protected $enteredByPersonID;

    /**
     *  @ORM\Column(name="Source", type="string", length=255, nullable=true)
     */
    protected $source;

    /**
     *  @ORM\Column(name="Notes", type="string", length=5000, nullable=true)
     */
    protected $notes;

    /**
     *  @ORM\Column(name="lastModified", type="datetime", nullable=true)
     */
    protected $lastModified;

    /**
     *  @ORM\Column(name="Active", type="integer", nullable=true)
     */
    protected $active;

    /**
     *  @ORM\Column(name="DoNotInclude", type="string", length=5000, nullable=true)
     */
    protected $doNotInclude;

    /**
     *  @ORM\Column(name="CompanyID", type="integer", nullable=true)
     */
    protected $companyID;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyName.
     *
     * @param string $companyName
     *
     * @return DoNotContact
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName.
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set companyNameAlternateList.
     *
     * @param string $companyNameAlternateList
     *
     * @return DoNotContact
     */
    public function setCompanyNameAlternateList($companyNameAlternateList)
    {
        $this->companyNameAlternateList = $companyNameAlternateList;

        return $this;
    }

    /**
     * Get companyNameAlternateList.
     *
     * @return string
     */
    public function getCompanyNameAlternateList()
    {
        return $this->companyNameAlternateList;
    }

    /**
     * Set creationDateTime.
     *
     * @param \DateTime $creationDateTime
     *
     * @return DoNotContact
     */
    public function setCreationDateTime($creationDateTime)
    {
        $this->creationDateTime = $creationDateTime;

        return $this;
    }

    /**
     * Get creationDateTime.
     *
     * @return \DateTime
     */
    public function getCreationDateTime()
    {
        return $this->creationDateTime;
    }

    /**
     * Set enteredByPersonID.
     *
     * @param int $enteredByPersonID
     *
     * @return DoNotContact
     */
    public function setEnteredByPersonID($enteredByPersonID)
    {
        $this->enteredByPersonID = $enteredByPersonID;

        return $this;
    }

    /**
     * Get enteredByPersonID.
     *
     * @return int
     */
    public function getEnteredByPersonID()
    {
        return $this->enteredByPersonID;
    }

    /**
     * Set source.
     *
     * @param string $source
     *
     * @return DoNotContact
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set notes.
     *
     * @param string $notes
     *
     * @return DoNotContact
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes.
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set lastModified.
     *
     * @param \DateTime $lastModified
     *
     * @return DoNotContact
     */
    public function setLastModified($lastModified)
    {
        $this->lastModified = $lastModified;

        return $this;
    }

    /**
     * Get lastModified.
     *
     * @return \DateTime
     */
    public function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * Set active.
     *
     * @param int $active
     *
     * @return DoNotContact
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return int
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set doNotInclude.
     *
     * @param string $doNotInclude
     *
     * @return DoNotContact
     */
    public function setDoNotInclude($doNotInclude)
    {
        $this->doNotInclude = $doNotInclude;

        return $this;
    }

    /**
     * Get doNotInclude.
     *
     * @return string
     */
    public function getDoNotInclude()
    {
        return $this->doNotInclude;
    }

    /**
     * Set companyID.
     *
     * @param int $companyID
     *
     * @return DoNotContact
     */
    public function setCompanyID($companyID)
    {
        $this->companyID = $companyID;

        return $this;
    }

    /**
     * Get companyID.
     *
     * @return int
     */
    public function getCompanyID()
    {
        return $this->companyID;
    }
}
