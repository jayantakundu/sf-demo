<?php

namespace Guidepoint\Bundle\ComplianceBundle\Repository;

use Guidepoint\Bundle\ComplianceBundle\Entity\DoNotContact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


class DncRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DoNotContact::class);
    }

}
