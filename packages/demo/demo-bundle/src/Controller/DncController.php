<?php

namespace Guidepoint\Bundle\ComplianceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Guidepoint\Bundle\ComplianceBundle\Component\DncManager;

class DncController extends AbstractController
{

    public function __construct(private DncManager $dncManager){
    }

    public function index(Request $request): Response
    {
        // $result = $this->get('gp_dnc_manager')->getDncCompanyData($request);
        $result = $this->dncManager->getDncCompanyData($request);
        // dd($result);
        return $this->json([
            "data"=>$result
        ]);
    }

    public static function getSubscribedServices()
    {
        $services = parent::getSubscribedServices();
        $services['gp_dnc_manager'] = '?Guidepoint\Bundle\ComplianceBundle\Component\DncManager';

        return $services;
    }
}
