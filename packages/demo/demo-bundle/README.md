# Guidepoint Compliance Bundle


Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### For Local

Add this below code to composer.json
```php
// 
"autoload": {
    "psr-4": {
      "Guidepoint\\Bundle\\ComplianceBundle\\": "packages/guidepoint/compliance-bundle/src/",
      // ...
    }
  },
```
To install "guidepoint/compliance-bundle" package add this below code to composer.json
```php
"repositories": [
        {
        "type": "path",
        "url": "./packages/guidepoint/compliance-bundle"
        }
    ],
```

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require guidepoint/compliance-bundle
```

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    Guidepoint\Bundle\ComplianceBundle\GuidepointComplianceBundle::class => ['all' => true],
];
```

### Step 3: Add Compliance Route 

Then, add route in the `config/routes/compliance_bundle.yml` folder of your project:

```php
// config/routes/compliance_bundle
guidepoint_compliance_bundle.routes:
  resource: '@GuidepointComplianceBundle/config/routes.yml'
```

### Step 4: Enable Access Controll to Route

Then, give permission to this route in the `config/packages/security.php` file of your project add below code under access_control:

```php
// config/packages/security.php
access_control:
....
- { path: '^/dnc$', roles: [ROLE_PROJECTMANAGER], methods: [GET]}
```
